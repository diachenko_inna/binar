<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tree".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $position
 * @property string $path
 * @property int $level
 *
 * @property Tree $parent
 * @property Tree[] $children
 * @property Tree $leftChild
 * @property Tree $rightChild
 */
class Tree extends \yii\db\ActiveRecord
{
    const POS_LEFT = 1;
    const POS_RIGHT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'position', 'level'], 'integer'],
            [['level'], 'required'],
            [['path'], 'string', 'max' => 12288],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tree::class, 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'position' => 'Position',
            'path' => 'Path',
            'level' => 'Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Tree::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Tree::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeftChild()
    {
        return $this->hasOne(Tree::class, ['parent_id' => 'id'])->where(['position' => 1]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightChild()
    {
        return $this->hasOne(Tree::class, ['parent_id' => 'id'])->where(['position' => 2]);
    }
}
