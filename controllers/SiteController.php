<?php

namespace app\controllers;

use app\models\Tree;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * Displays homepage with tree.
     *
     * @return string
     */
    public function actionIndex()
    {
        $tree = Yii::$app->treeManager->treeCreator->getFirstNode();

        return $this->render('index', [
            'tree' => $tree
        ]);
    }

    /**
     * Displays tables data.
     *
     * @return string
     */
    public function actionTable()
    {
        $treeProvider = new ActiveDataProvider([
            'query' => Tree::find()
        ]);

        return $this->render('table', [
            'treeProvider' => $treeProvider
        ]);
    }

    /**
     * Clear tree
     *
     * @return \yii\web\Response
     */
    public function actionClearTree()
    {
      Yii::$app->treeManager->clearTree();

      return $this->redirect('index');
    }

    /**
     * Fill tree
     *
     * @return \yii\web\Response
     */
    public function actionFillTree()
    {
      Yii::$app->treeManager->fillTree();

      return $this->redirect('index');
    }

    /**
     * Create new tree node
     *
     * @param $parentId integer
     * @param $position integer
     * @return \yii\web\Response
     */
    public function actionAddNode($parentId, $position)
    {
        Yii::$app->treeManager->treeCreator->createNode($parentId, $position);

        return $this->redirect('index');
    }

    /**
     * Action for node moving
     *
     * @param $id
     * @param $parentId
     * @param $position
     * @return \yii\web\Response
     */
    public function actionMoveNode($id, $parentId, $position)
    {
        Yii::$app->treeManager->moveNode($id, $parentId, $position);

        return $this->redirect('index');
    }
}
