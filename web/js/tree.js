$(document).ready(function (e) {
    $('.item').draggable({
        zIndex: 100,
        revert: true
    });
});

$( ".new-node" ).droppable({
    accept: ".item",
    activeClass: "active",
    hoverClass: "drop-hover",
    drop: function(e, item) {
        let node = e.target;
        let id = item.draggable.data('id');
        let parentId = $(node).data('id');
        let position = $(node).data('position');
        if (id !== parentId) {
            window.location = '/site/move-node?id=' + id + '&parentId=' + parentId + "&position=" + position;
        }
    }
});
