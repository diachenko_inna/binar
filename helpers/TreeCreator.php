<?php

namespace app\helpers;

use app\models\Tree;

/**
 * Class TreeCreator
 * For adding new node to binary tree
 *
 * @package app\helpers
 */
class TreeCreator
{
    const MIN_LEVEL = 1;

    /**
     * @var Tree|bool
     */
    private $firstNode;

    /**
     * TreeCreator constructor.
     */
    public function __construct()
    {
        $this->firstNode = Tree::findOne(['level' => self::MIN_LEVEL]);

        if (!$this->firstNode) {
            $this->firstNode = $this->createFirstNode();
        }
    }

    /**
     * Create head node for binary tree
     * if creation successfully new Tree model will be return
     * in other case - false status
     *
     * @return Tree|bool
     */
    public function createFirstNode()
    {
        // create first node
        $node = $this->saveModel([
            'level' => self::MIN_LEVEL,
            'path' => ''
        ]);

        $this->firstNode = $node;

        return $node;
    }

    /**
     * Create new node in tree by its parent id and position
     * if creation successfully new Tree model will be return
     * in other case - false status
     *
     * @param int $parentId
     * @param int $position
     * @return Tree|bool
     */
    public function createNode(int $parentId, int $position)
    {
        // find parent node
        $parent = Tree::findOne($parentId);

        // create new children node
        $node = $this->saveModel([
            'parent_id' => $parentId,
            'position' => $position,
            'level' => $parent->level + 1,
            'path' => $parent->path
        ]);

        return $node;
    }

    /**
     * Create and save new Tree model based on specified attributes
     *
     * @param array $attributes
     * @return Tree|bool
     */
    private function saveModel(array $attributes)
    {
        // create new tree
        $node = new Tree($attributes);

        // validate tree model
        if (!$node->validate()) {
            return false;
        }

        // add current id to node path
        $node->save();
        $node->path .= ($node->level > self::MIN_LEVEL ? "." : '') . $node->id;

        if ($node->save()) {
            return $node;
        }

        return false;
    }

    /**
     * Getter for firstNode field
     *
     * @return Tree
     */
    public function getFirstNode()
    {
        if (empty($this->firstNode)) {
            $this->createFirstNode();
        }

        return $this->firstNode;
    }
}
