<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tree}}`.
 * Has foreign key to the table tree
 */
class m191027_195629_create_tree_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tree}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'position' => $this->integer(),
            'path' => $this->string(12288),
            'level' => $this->integer()->notNull()
        ]);

        // creates index for column parent_id
        $this->createIndex(
            'idx-tree-parent_id',
            'tree',
            'parent_id'
        );

        // add foreign key for table tree
        $this->addForeignKey(
            'fk-tree-parent_id',
            'tree',
            'parent_id',
            'tree',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table tree
        $this->dropForeignKey(
            'fk-tree-parent_id',
            'tree'
        );

        // drops index for column parent_id
        $this->dropIndex(
            'idx-tree-parent_id',
            'tree'
        );
        $this->dropTable('{{%tree}}');
    }
}
