<?php

use app\models\Tree;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $node Tree */
/* @var $parentId integer */
/* @var $position integer */

$this->title = Yii::$app->name;
$nodeType = (empty($parentId) ? 'head' : ($position === Tree::POS_LEFT ? 'left item' : 'right item'));
?>
<div class="node-item">
    <?php if (!empty($node)) :?>
        <div class="<?= 'node-content ' . $nodeType ?>" data-id="<?= $node->id ?>">
            <div>
                <?= $node->id ?>
            </div>
        </div>
    <?php else:?>
        <div class="node-content new-node" data-id="<?= $parentId ?>" data-position="<?= $position ?>">
            <a href="<?= Url::to(['/site/add-node', 'parentId' => $parentId, 'position' => $position]) ?>">
                <span class="glyphicon glyphicon-plus"></span>
            </a>
        </div>
    <?php endif;?>
</div>
