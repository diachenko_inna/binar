<?php

/* @var $this yii\web\View */
/* @var $tree app\models\Tree */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <h1>Model Tree Structure with Materialized Paths</h1>
            <div class="examples">
                <div class="example">
                    <div class="node-content head"></div>
                    <span class="tip"> - HEAD node</span>
                </div>
                <div class="example">
                    <div class="node-content left example"></div>
                    <span class="tip"> - LEFT node</span>
                </div>
                <div class="example">
                    <div class="node-content right example"></div>
                    <span class="tip"> - RIGHT node</span>
                </div>
            </div>
            <span>*You can move Blue and Rose nodes.</span>
            <hr/>
            <?= $this->render('node', ['node' => $tree]);?>
            <?= $this->render('tree', ['tree' => $tree]);?>
        </div>
    </div>
</div>
