<?php

/* @var $this yii\web\View */
/* @var $treeProvider \yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <?= \yii\grid\GridView::widget(['dataProvider' => $treeProvider]); ?>
            </div>
        </div>
    </div>
</div>
