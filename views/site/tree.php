<?php

use \app\models\Tree;

/* @var $this yii\web\View */
/* @var $tree \app\models\Tree */

$this->title = Yii::$app->name;
?>
<div class="tree">
    <div class="row">
        <div class="col-xs-6">
            <?= $this->render('node', [
                    'node' => $tree->leftChild,
                    'parentId' => $tree->id,
                    'position' => Tree::POS_LEFT
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $this->render('node', [
                    'node' => $tree->rightChild,
                    'parentId' => $tree->id,
                    'position' => Tree::POS_RIGHT
                ]) ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-xs-6">
            <?= $tree->leftChild ? $this->render('tree', ['tree' => $tree->leftChild]) : ''; ?>
        </div>
        <div class="col-xs-6">
            <?= $tree->rightChild ? $this->render('tree', ['tree' => $tree->rightChild]) : '' ?>
        </div>
    </div>
</div>
