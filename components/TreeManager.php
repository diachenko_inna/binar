<?php

namespace app\components;

use app\helpers\TreeCreator;
use app\models\Tree;
use Yii;
use yii\base\Component;

/**
 * This is component for operations with binary tree
 *
 * @package app\components
 */
class TreeManager extends Component
{
    public $treeCreator;

    /**
     * Init function for TreeManager component
     * creates new treeCreator
     */
    public function init()
    {
        $this->treeCreator = new TreeCreator();
        parent::init();
    }

    /**
     * Fill tree
     *
     * @param int $level
     * @param int $parentId
     */
    public function fillTree(int $level = 5, ?int $parentId = null)
    {
        // indicate head node
        $head = $parentId
            ? Tree::findOne($parentId)
            : $this->treeCreator->getFirstNode();

        // clear existing tree
        $this->clearTree($head->id);

        // build tree
        $this->buildTree($head->id, $level - 1);
    }

    /**
     * Recursively create new tree
     *
     * @param int $headId
     * @param int $level
     */
    private function buildTree(int $headId, int $level)
    {
        if ($level === 0) {
            return;
        }

        $leftChild = $this->treeCreator->createNode($headId, Tree::POS_LEFT);
        $rightChild = $this->treeCreator->createNode($headId, Tree::POS_RIGHT);

        if ($leftChild) {
            $this->buildTree($leftChild->id, $level - 1);
        }

        if ($rightChild) {
            $this->buildTree($rightChild->id, $level - 1);
        }
    }

    /**
     * Get all children for node
     *
     * @param int $nodeId
     * @return Tree[]
     */
    public function getChildren(int $nodeId)
    {
        return Tree::findAll(['like', 'path', '%.' . $nodeId . '.%']);
    }

    /**
     * Get all parents for node
     *
     * @param int $nodeId
     * @return Tree[]|null
     */
    public function getAllParents(int $nodeId)
    {
        $node = Tree::findOne($nodeId);

        if (!$node) {
            return null;
        }

        $parentsIds = explode('.', substr($node->path, 0, -strlen($node->id)));

        return Tree::findAll(['id' => $parentsIds]);
    }

    /**
     * Move node with its tree to other parent
     *
     * @param int $id
     * @param int $parentId
     * @return int
     */
    public function moveNode(int $id, int $parentId, int $position)
    {
        $parent = Tree::findOne($parentId);

        if (in_array($id, explode('.', $parent->path))) {
            return false;
        }

        return Tree::updateAll([
            'parent_id' => $parentId,
            'position' => $position
        ], ['id' => $id]);
    }

    /**
     * Remove specified tree
     *
     * @param int|null $nodeId
     */
    public function clearTree(?int $nodeId = null)
    {
        if ($nodeId) {
            // remove sub tree
            Tree::deleteAll(['parent_id' => $nodeId]);
        } else {
            // remove whole tree
            Tree::deleteAll(['>', 'id', 0]);
            try {
                // reset id increment
                Yii::$app->db->createCommand('ALTER TABLE {{%tree}} AUTO_INCREMENT = 1')->execute();
            } catch (\Exception $exception) {
                var_dump($exception->getMessage());
            }
        }
    }
}
